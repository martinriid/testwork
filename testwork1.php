<?php
$txtFile = "data.txt"; //txt file name
$txtArray = file($txtFile); //data from the txt file -> array
$dataArray = array();
foreach ($txtArray as $row) { 
	$data = explode('|', $row, 3); //split string using | as a delimiter
	$dataArray[] = array('node_id'=>$data[0], 'parent_id'=>$data[1], 'node_name'=>$data[2]); //fill the dataArray array with data from the txt file
}
asort($dataArray); //sort the array by node_id
$mainArray = array();
foreach ($dataArray as $value) { 
	$mainArray[$value['parent_id']][] = $value;
}
$treeArray = createArrayTree($mainArray, $mainArray[0]); //create tree from array
displayArrayTree($treeArray); //display array
function createArrayTree(&$fullarray, $array){ 
foreach ($array as $key => $value) {
		if (isset($fullarray[$value['node_id']])) { //if the array is a parent then add a children array to it and recall the function
			$value['children'] = createArrayTree($fullarray, $fullarray[$value['node_id']]);
		}
		$tree[] = $value;
		
	}
	return $tree;
}
function displayArrayTree($array, $depth = 0) {
	foreach ($array as $key => $value) { 
		if (is_array($value)) { //if it's an array add depth and recall the function
			displayArrayTree($value, $depth+0.5);
		} else if (!is_numeric($value)) { //if it's not numeric nor array, then display it
			echo str_repeat("-", $depth)." ".$value."<br>"; 
		}
	}
}
?>